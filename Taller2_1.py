# This is a sample Python script.

# Jose Andres Perea Camelo - 34922.

booleanos = [False, True]

# Tabla de verdad de or, and y xor

print(' a\t     b\t     c\t     d\t     e\t     Or\t     And\t Xor')
print('-' * 63)
for a in booleanos:
    for b in booleanos:
        for c in booleanos:
            for d in booleanos:
                for e in booleanos:
                    print(a, b, c, d, e, a or b or c or d or e, a and b and c and d and e, a ^ b ^ c ^ d ^ e, sep='\t')
